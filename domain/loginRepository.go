package domain

import (
	"database/sql"
	"errors"
	"log"

	"github.com/jmoiron/sqlx"
)

type AuthRepository interface {
	FindBy(username, password string) (*Login, error)
}

type AuthRepositoryDB struct {
	client *sqlx.DB
}

func (d AuthRepositoryDB) FindBy(username, password string) (*Login, error) {
	var login Login
	sqlVerify := `SELECT username, u.customer_id, role, group_concat(a.account_id) as account_numbers 
	FROM users u LEFT JOIN accounts a ON a.customer_id = u.customer_id 
	WHERE username = ? and password = ? GROUP BY a.customer_id`

	err := d.client.Get(&login, sqlVerify, username, password)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("Invalid crendentials")
		} else {
			log.Println("Error while verifying login request from database: ", err)
			return nil, errors.New("Unexpected database error")
		}
	}

	return &login, nil
}

func NewAuthRepository(client *sqlx.DB) AuthRepositoryDB {
	return AuthRepositoryDB{client}
}
