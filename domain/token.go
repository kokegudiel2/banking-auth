package domain

import (
	"encoding/json"

	"github.com/dgrijalva/jwt-go"
)

const HMAC_SAMPLE_SECRET = "hmacSampleSecret"

type Claims struct {
	CustomerID string   `json:"customer_id"`
	Accounts   []string `json:"accounts"`
	Username   string   `json:"username"`
	Expiry     int64    `json:"exp"`
	Role       string   `json:"role"`
}

func (c Claims) IsUserRole() bool {
	return c.Role == "user"
}

func BuildClaimsFromJwtMapClaims(mapClaims jwt.MapClaims) (*Claims, error) {
	bytes, err := json.Marshal(mapClaims)
	if err != nil {
		return nil, err
	}

	var c Claims
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}

func (c Claims) IsValidCustomerID(customerID string) bool {
	return c.CustomerID == customerID
}

func (c Claims) IsValidAccountID(accountID string) bool {
	if accountID != "" {
		accountFound := false
		for _, a := range c.Accounts {
			if a == accountID {
				accountFound = true
				break
			}
		}
		return accountFound
	}

	return true
}

func (c Claims) IsRequestVerifiedWithTokenClaims(urlParams map[string]string) bool {
	if c.CustomerID != urlParams["customer_id"] {
		return false
	}

	if !c.IsValidAccountID(urlParams["account_id"]) {
		return false
	}

	return true
}
