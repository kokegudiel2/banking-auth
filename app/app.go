package app

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql" //Driver mariadb

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kokegudiel2/banking-auth/domain"
	"gitlab.com/kokegudiel2/banking-auth/service"
)

func Start() {
	sanityCheck()
	router := mux.NewRouter()
	authRepository := domain.NewAuthRepository(getDBClient())
	ah := AuthHandler{service: service.NewLoginService(authRepository, domain.GetRolePermissions())}

	router.HandleFunc("/auth/login", ah.Login).Methods("POST")
	router.HandleFunc("/auth/register", ah.NoImplementedHandler).Methods("POST")
	router.HandleFunc("/auth/verify", ah.Verify).Methods("GET")

	log.Printf("Starting OAuth server on %s:%s ...\n", address, port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func getDBClient() *sqlx.DB {
	dataSource := fmt.Sprintf("%s:%s@tcp(%s)/%s", dbUser, dbPasswd, dbAddr, dbName)
	client, err := sqlx.Open("mysql", dataSource)
	if err != nil {
		panic(err)
	}
	// See "Important settings" section.
	client.SetConnMaxLifetime(time.Minute * 3)
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)
	return client
}

var (
	address  = os.Getenv("SERVER_ADDRESS")
	port     = os.Getenv("SERVER_PORT")
	dbUser   = os.Getenv("DB_USER")
	dbPasswd = os.Getenv("DB_PASSWD")
	dbAddr   = os.Getenv("DB_ADDR")
	dbName   = os.Getenv("DB_NAME")
)

func sanityCheck() {
	envProps := []string{
		"SERVER_ADDRESS",
		"SERVER_PORT",
		"DB_USER",
		"DB_PASSWD",
		"DB_ADDR",
		"DB_NAME",
	}
	for _, k := range envProps {
		if os.Getenv(k) == "" {
			log.Fatalf("Environment variable %s not defined. Terminating application...", k)
		}
	}
}
